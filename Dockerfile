FROM python:3.6-alpine

RUN apk --update add git g++ gcc libxslt-dev

EXPOSE 80

COPY . /code
WORKDIR /code
RUN pip install -r requirements.txt
RUN pip install gunicorn

CMD gunicorn eurnik_ical.app:app --bind 0.0.0.0:${PORT:-80}

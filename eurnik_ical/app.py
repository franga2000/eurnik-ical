import traceback
from os import environ, path

try:
	import eurnik_ical.eurnik_ical_util as eurnik_ical_util
except ImportError:
	# `flask run` is strange
	import eurnik_ical_util as eurnik_ical_util

from flask import Flask, Response, make_response, render_template, request
from werkzeug.contrib.fixers import ProxyFix

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)

if "SENTRY_DSN" in environ:
	from raven.contrib.flask import Sentry
	from raven.versioning import fetch_git_sha
	import logging
	sentry = Sentry(app, dsn=environ["SENTRY_DSN"], logging=True, level=logging.WARNING)

	sentry.client.include_paths |= set(["easistent", "icalendar", "flask", "lxml"])
	sentry.client.release = fetch_git_sha(path.dirname(path.dirname(__file__)))

	eurnik_ical_util.sentry = sentry

@app.route("/")
def index():
	"""Glavna stran."""
	ical_url = error = None
	if "url" in request.args and request.args["url"]:
		if request.args["url"].startswith("https://www.easistent.com/urniki/"):
			try:
				info = eurnik_ical_util.info_from_url(request.args["url"])
				ical_url = eurnik_ical_util.build_url(info)
			except Exception as e:
				error = traceback.format_exc()
		else:
			error = "Vaš URL je neveljaven"
	return render_template("ea_url.html", ical_url=ical_url, error=error)

@app.route("/ical/<sola>/<razred>/urnik.ics")
def ical_urnik(sola, razred):
	"""Urnik celega razreda."""
	return eurnik_ical_util.multi_urnik(sola, razred)

@app.route("/ical/<sola>/<razred>/<dijak>/urnik.ics")
def ical_urnik_dijak(sola, razred, dijak):
	"""Urnik specifičnega dijaka."""
	return eurnik_ical_util.multi_urnik(sola, razred, dijak)

def mime_ical(ical: str) -> Response:
	"""iCal besedilo pretvori v Response objekt z mimetype="text/calendar"."""
	r = make_response(ical)
	r.mimetype = "text/calendar"
	return r

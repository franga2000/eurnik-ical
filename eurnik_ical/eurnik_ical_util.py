import re
import traceback
import uuid
from datetime import datetime

import lxml.html
import requests

from easistent import urnik as eurnik
from easistent import util
from flask import request, url_for
from icalendar import Calendar, Event, vCalAddress, vText

import logging
logger = logging.getLogger(__name__)

sentry = None
exceptions = []

def exception_is_new(exception: Exception) -> bool:
	for e in exceptions:
		if str(exception) == str(e):
			return False

	exceptions.append(exception)
	return True

def info_from_url(url: str) -> dict:
	"""Iz urnika na naslovu `url` prebere podatke o šoli."""
	# TODO: Premakni v knjižnico
	html = requests.get(url).text
	dom = lxml.html.fromstring(html)

	scripts = dom.cssselect("script")
	blob = "\n".join([script.text for script in scripts if script.text])

	regex = r"(var (?P<name>[\w_]+) = '(?P<value>\d+)')+"
	matches = re.finditer(regex, blob)

	params = dict([(m.group("name"), m.group("value")) for m in matches])
	return {
		"šola": params["id_sola"],
		"dijak": params["id_dijak"],
		"razred": params["id_razred"]
	}

def build_url(info: dict) -> str:
	"""Sestavi URL iz danih podatkov (glej `info_from_url`)."""
	if info["dijak"] != '0':
		path = url_for("ical_urnik_dijak", sola=info["šola"], razred=info["razred"], dijak=info["dijak"])
	else:
		path = url_for("ical_urnik", sola=info["šola"], razred=info["razred"])
	return request.url_root + path.strip("/")

def teden_range(teden: int, minus: int, plus: int):
	"""Vrne vse številke tednov od `teden-minus` do `teden+plus` ki obstajajo (<=53 && >=1)."""
	if teden > 50:
		teden = 1
	tedni = []
	od = max(1, teden - minus)
	do = min(53, teden + plus)
	tedni += range(od, do)
	return tedni

def new_cal() -> Calendar:
	"""Pripravi nov, prazen koledar."""
	cal = Calendar()
	cal.add("prodid", "eUrnik iCal")
	cal.add("version", "2.0")
	cal.add("X-WR-CALNAME", "eAsistent Urnik")
	return cal

#: Privzete ikone za vrste ur
default_ikone = {
	"Nadomeščanje": "(N)",
	"Dogodek": "(D)",
	"Šolski koledar": "(D)",
	"Odpadla ura": "(O)",
	"Zaposlitev": "(Z)"
	# TODO: More of these
}

def generate_ical(urnik, ikone=default_ikone):
	"""Pretvori eUrnik objekt v iCal format."""
	cal = new_cal()
	for ura in urnik:
		try:
			evt = Event()
			evt.add("dtstamp", datetime.now())
			evt.add("uid", uuid.uuid4())  # TODO: Better UID field

			naslov = ura["predmet"]
			if ura["vrsta"]:
				if ura["vrsta"] in ikone:
					naslov += " " + ikone[ura["vrsta"]]
				else:
					msg = "Neznana vrsta: " + ura["vrsta"]
					if exception_is_new(msg):
						logger.warning(msg)
			evt.add("summary", naslov)

			opis = ura["predmet_polno"]
			if ura["vrsta"]:
				opis += " (%s)" % ura["vrsta"]
			evt.add("description", opis)

			if "učilnica" in ura:
				evt.add("location", ura["učilnica"])

			evt.add("dtstart", ura["začetek"])
			evt.add("dtend", ura["konec"])

			if "profesor" in ura:
				organizer = vCalAddress("MAILTO:nobody@example.com")
				organizer.params["cn"] = vText(ura["profesor"])
				evt.add("organizer", organizer)  # TODO: GCal doesn't show this. My add it to the description

			cal.add_component(evt)

		except Exception as e:
			if exception_is_new(e):
				traceback.print_exc()
				if sentry:
					sentry.captureException()

	del exceptions[:]
	ical = cal.to_ical()  # TODO: Some iCal parsers complain about this. Figure out why!
	return ical

def multi_urnik(sola, razred, dijak='0', tedni=teden_range(util.teden(), 5, 5)) -> str:
	"""Skupaj združi urnike večih tednov."""
	urnik = []
	for t in tedni:
		urnik += eurnik.get(šola=sola, razred=razred, dijak=dijak, teden=t)
	ical = generate_ical(urnik)
	return ical
